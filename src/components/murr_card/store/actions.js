import axios from "axios";

import {
  STATUS_200_OK,
  STATUS_204_NO_CONTENT,
} from "../../../utils/http_response_status.js";

import * as type from "./type.js";

const handlerError = (error) => {
  let obj = { success: false };

  if (error.response && error.response.data && error.response.data.detail) {
    obj.message = error.response.data.detail;
  } else {
    obj.message = "Что-то пошло не по плану 😮";
  }

  return obj;
};

const MURR_CARDS_START_PAGE = 1;

export default {
  async [type.MURR_CARD_FETCH_LIST](
    { commit },
    { page = MURR_CARDS_START_PAGE }
  ) {
    try {
      const { data, status } = await axios.get(`/api/murr_card/?page=${page}`);

      if (status === STATUS_200_OK) {
        commit(type.MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerError(error);
    }
  },
  async [type.MURR_CARD_MY_LIKES](
    { commit, getters },
    { page = MURR_CARDS_START_PAGE }
  ) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.get(
        `/api/murr_card/my_likes/?page=${page}`,
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      if (status === STATUS_200_OK) {
        commit(type.MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerError(error);
    }
  },

  async [type.MURR_CARD_LIKED]({ getters }, { murrID }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.post(
        `/api/murr_card/${murrID}/like/`,
        {},
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      return { success: true, status, data };
    } catch (error) {
      return handlerError(error);
    }
  },

  async [type.MURR_CARD_UNLIKED]({ getters }, { murrID }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.post(
        `/api/murr_card/${murrID}/dislike/`,
        {},
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      return { success: true, status, data };
    } catch (error) {
      return handlerError(error);
    }
  },

  async [type.MURR_CARD_FETCH_ONE]({ commit }, { murrID }) {
    try {
      const { data, status } = await axios.get(`/api/murr_card/${murrID}/`);

      if (status === STATUS_200_OK) {
        commit(type.MURR_CARD_SET, data);

        return { success: true, data };
      }

      return { success: false, data };
    } catch (error) {
      return handlerError(error);
    }
  },

  async [type.MURR_CARD_DELETE]({ commit, getters }, { murrID }) {
    try {
      const token = getters.accessToken_getters;

      const { data, status } = await axios.delete(`/api/murr_card/${murrID}/`, {
        // todo: Fix - send to global instance of axios
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (status === STATUS_204_NO_CONTENT) {
        commit(type.MURR_CARD_CLEAR);

        return { success: true, message: "Мурр успешно удален!" };
      }

      return { success: false, message: data.detail };
    } catch (error) {
      return handlerError(error);
    }
  },
};
