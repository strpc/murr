import actions from "./actions.js";
import mutations from "./mutations.js";

export default {
  state: {
    profileData: null,
  },
  getters: {},
  actions,
  mutations,
};
