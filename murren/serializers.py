from rest_framework import serializers
from django.conf import settings

from .models import Murren


class MurrenSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()

    class Meta:
        model = Murren
        fields = ('id', 'username', 'email', 'murren_avatar', 'avatar')
        read_only_fields = ('avatar', )

    def get_avatar(self, obj):
        return f'{settings.BACKEND_URL}{obj.murren_avatar.url}'
