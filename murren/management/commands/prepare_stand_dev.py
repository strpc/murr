from django.core.management import BaseCommand
from django.contrib.auth import get_user_model

from murr_card.models import Category, MurrCard, MurrCardStatus

Murren = get_user_model()


class Command(BaseCommand):
    help = 'Подготовить свежий стенд для разработки / Prepare fresh stand for develop'
    CATEGORIES = {'Аниме': 'anime', 'Кодинг': 'coding', 'Игры': 'game', 'Other': 'other'}

    def handle(self, *args, **options):
        admin = Murren.objects.create_superuser('admin', 'admin@admin.com', 'admin')
        if admin:
            print("Администратор создан успешно - креды - admin/admin")

        murren = Murren.objects.create_user('Greg', 'admin1@admin.com', '1q2w3e!')
        if murren:
            print("Муррен создан - креды - Greg/1q2w3e!")

        for category in self.CATEGORIES:
            Category.objects.create(name=category, slug=self.CATEGORIES[category])
            print(f'Создана категория {category}')

        murr = MurrCard.objects.create(
            title='Это тестовый мурр. Он был создан при вызове команды prepare_stand_dev. '
                  'Желаю удачной разработки и крепкого  здоровья',
            owner=murren, cover='tanochka.jpg', status=MurrCardStatus.RELEASE)
        if murr:
            print("Тестовый мурр создан")
