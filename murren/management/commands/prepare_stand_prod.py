from django.core.management import BaseCommand
from django.contrib.auth import get_user_model

from murr_card.models import Category

Murren = get_user_model()


class Command(BaseCommand):
    help = 'Подготовить свежий стенд / Prepare fresh stand'
    CATEGORIES = {'Аниме': 'anime', 'Кодинг': 'coding', 'Игры': 'game', 'Other': 'other'}

    def handle(self, *args, **options):
        for category in self.CATEGORIES:
            Category.objects.create(name=category, slug=self.CATEGORIES[category])
            print(f'Создана категория {category}')
