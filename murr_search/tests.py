import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


@pytest.mark.django_db
@pytest.mark.parametrize('count', [3])
def test_murr_card_search(create_release_murr_list):
    murr_card_list = create_release_murr_list['murr_card_list']
    api = APIClient()

    expected_index = 2
    url = f'{reverse("search_murr_card")}?search={expected_index}'
    response = api.get(url)
    resp_json = response.json()
    result_object = resp_json['results'][0]
    expected_murr_card = murr_card_list[expected_index]
    assert response.status_code == status.HTTP_200_OK
    assert resp_json['count'] == 1
    assert result_object['id'] == expected_murr_card.id
    assert result_object['title'] == expected_murr_card.title
    assert result_object['content'] == expected_murr_card.content


@pytest.mark.django_db
@pytest.mark.parametrize('count', [3])
def test_murren_search(create_murren_list):
    murren_list = create_murren_list
    api = APIClient()

    expected_index = 1
    url = f'{reverse("search_murren")}?search={expected_index}'
    response = api.get(url)
    resp_json = response.json()
    result_object = resp_json['results'][0]
    expected_murren = murren_list[expected_index]
    assert response.status_code == status.HTTP_200_OK
    assert resp_json['count'] == 1
    assert result_object['id'] == expected_murren.id


@pytest.mark.django_db
@pytest.mark.parametrize('count', [3])
def test_murr_card_wrong_search(create_release_murr_list):
    api = APIClient()

    url = f'{reverse("search_murr_card")}?search=ABRAKADABRA'
    response = api.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 0


@pytest.mark.django_db
@pytest.mark.parametrize('count', [3])
def test_murren_wrong_search(create_murren_list):
    api = APIClient()

    url = f'{reverse("search_murren")}?search=ABRAKADABRA'
    response = api.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 0
