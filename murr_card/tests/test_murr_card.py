import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from murr_card.models import MurrCard


@pytest.mark.django_db
def test_murr_card_create(create_murren, create_category, murr_card_data):
    murren = create_murren
    category = create_category
    api = APIClient()
    api.force_authenticate(user=murren)

    data = {
        **murr_card_data,
        'owner': murren.username,
        'category': category.slug
    }

    url = reverse('murr_card-list')
    response = api.post(url, data, format='json')
    murr_card = MurrCard.objects.first()
    murr_card_tags = murr_card.tags.values_list('name', flat=True)
    assert response.status_code == status.HTTP_201_CREATED
    assert MurrCard.objects.count() == 1
    assert murr_card.owner == murren
    assert murr_card.title == data['title']
    assert murr_card.content == data['content']
    assert murr_card.category == category
    assert all(tag in murr_card_tags for tag in data['tags'])


@pytest.mark.django_db
def test_murr_card_create_not_authenticated(create_murren, murr_card_data):
    murren = create_murren
    api = APIClient()

    data = {
        **murr_card_data,
        'owner': murren.username,
    }

    url = reverse('murr_card-list')
    response = api.post(url, data=data, format='json')
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_murr_card_delete(create_release_murr):
    murren = create_release_murr['murren']
    api = APIClient()
    api.force_authenticate(user=murren)

    assert MurrCard.objects.count() == 1

    url = reverse('murr_card-detail', kwargs={'pk': create_release_murr['murr_card'].id})
    response = api.delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert MurrCard.objects.count() == 0


@pytest.mark.django_db
def test_murr_card_delete_not_authenticated(create_release_murr):
    api = APIClient()
    url = reverse('murr_card-detail', kwargs={'pk': create_release_murr['murr_card'].id})
    response = api.delete(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
@pytest.mark.parametrize('count', [3])
def test_murr_card_list(create_release_murr_list):
    murren = create_release_murr_list['murren']
    api = APIClient()
    api.force_authenticate(user=murren)

    url = reverse('murr_card-list')
    response = api.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == MurrCard.objects.count()


@pytest.mark.django_db
@pytest.mark.parametrize('count', [3])
def test_murr_card_my_likes_list(create_release_murr_list):
    murr_cards = create_release_murr_list['murr_card_list']
    murren = create_release_murr_list['murren']
    api = APIClient()
    api.force_authenticate(user=murren)

    assert MurrCard.objects.count() == 3

    url = reverse('murr_card-like', kwargs={'pk': murr_cards[0].id})
    assert api.post(url).status_code == 200

    url = reverse('murr_card-my-likes')
    response = api.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == murren.liked_murrcards.count()


@pytest.mark.django_db
def test_murr_card_my_likes_list_not_authenticated():
    api = APIClient()

    url = reverse('murr_card-my-likes')
    response = api.get(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_set_default_category(create_murren, create_category, murr_card_data):
    murren = create_murren
    category = create_category
    api = APIClient()
    api.force_authenticate(user=murren)

    data = {
        **murr_card_data,
        'owner': murren.username,
        'category': '',
    }

    url = reverse('murr_card-list')
    response = api.post(url, data, format='json')
    murr_card = MurrCard.objects.first()
    assert response.status_code == status.HTTP_201_CREATED
    assert murr_card.category == category
