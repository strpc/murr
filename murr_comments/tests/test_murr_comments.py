import pytest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient


@pytest.mark.django_db
def test_murren_create_comment_for_murr(create_release_murr, murr_comment_data):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api = APIClient()
    api.force_authenticate(user=murren)

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
    }

    url = reverse('comment-list')
    response = api.post(url, data, format='json')
    assert response.status_code == status.HTTP_201_CREATED

    url = reverse('comment-detail', kwargs={'pk': response.json()['id']})
    response = api.get(url)
    murr_comment = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert murr_comment['author_username'] == murren.username
    assert murr_comment['murr'] == murr.pk
    assert murr_comment['text'] == data['text']


@pytest.mark.django_db
def test_murren_create_comment_for_murr_not_authenticated(create_release_murr, murr_comment_data):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api = APIClient()

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
    }

    url = reverse('comment-list')
    response = api.post(url, data, format='json')
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_murren_create_comment_for_comment(create_murr_comment, murr_comment_data):
    murren = create_murr_comment['murren']
    murr = create_murr_comment['murr_card']
    murr_comment = create_murr_comment['murr_comment']
    api = APIClient()
    api.force_authenticate(user=murren)

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
        "parent": murr_comment.pk,
    }

    url = reverse('comment-list')
    response = api.post(url, data, format='json')
    children_comment = response.json()
    assert response.status_code == status.HTTP_201_CREATED

    url = reverse('comment-detail', kwargs={'pk': murr_comment.pk})
    response = api.get(url)
    murr_comment = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert murr_comment['children'][0] == children_comment


@pytest.mark.django_db
def test_murren_create_comment_for_comment_not_authenticated(create_murr_comment, murr_comment_data):
    murren = create_murr_comment['murren']
    murr = create_murr_comment['murr_card']
    murr_comment = create_murr_comment['murr_comment']
    api = APIClient()

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
        "parent": murr_comment.pk,
    }

    url = reverse('comment-list')
    response = api.post(url, data, format='json')
    children_comment = response.json()
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_create_comment_when_murren_is_banned(create_murren_is_banned, create_release_murr, murr_comment_data):
    murren = create_murren_is_banned
    murr = create_release_murr['murr_card']
    api = APIClient()
    api.force_authenticate(user=murren)

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
    }

    url = reverse('comment-list')
    response = api.post(url, data, format='json')
    assert response.status_code == status.HTTP_403_FORBIDDEN
